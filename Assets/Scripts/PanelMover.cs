using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelMover : MonoBehaviour
{
    [SerializeField] private float m_speed;
    [SerializeField] private float m_destroyDepth = -5;

    // Update is called once per frame
    void Update()
    {
        transform.position = transform.position - Vector3.forward * Time.deltaTime * m_speed;
        if (transform.position.z < -m_destroyDepth)
        {
            Destroy(this.gameObject);
        }
    }
}
