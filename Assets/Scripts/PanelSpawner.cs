using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelSpawner : MonoBehaviour
{
    [SerializeField] private List<GameObject> m_panelList;
    [SerializeField] private float m_spawnDelay;
    [SerializeField] private Vector3 m_spawnLocation;

    private void Start()
    {
        StartCoroutine(SpawnPanelsCoroutine());
    }

    IEnumerator SpawnPanelsCoroutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(m_spawnDelay);
            OnSpawn(0.5f);
        }
    }

    private void OnSpawn(float doorPosition)
    {
        int panelId = Random.Range(0, m_panelList.Count - 1);
        Instantiate(m_panelList[panelId], m_spawnLocation, Quaternion.identity);
    }

}
