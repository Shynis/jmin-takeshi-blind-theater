using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSilhouette : MonoBehaviour
{
    [SerializeField] private bool displaySilhouette = true;

    private SpriteRenderer silhouetteSprite;
    private Vector3 silhouettePosition;

    private PositionManager positionManager;

    private void Awake()
    {
        positionManager = FindObjectOfType<PositionManager>();
        silhouetteSprite = GetComponentInChildren<SpriteRenderer>();
    }

    private void Start()
    {
        silhouettePosition = new Vector3(0, 0, 0);
    }

    private void Update()
    {
        if(displaySilhouette && positionManager.headFound)
        {
            silhouetteSprite.enabled = true;
            MoveSilhouette();
        }
        else
        {
            silhouetteSprite.enabled = false;
        }
    }

    private void MoveSilhouette()
    {
        Vector2 headPosition = positionManager.headPosition;

        silhouettePosition.x = headPosition.x;
        silhouettePosition.y = headPosition.y;
        silhouettePosition.z = 0;

        this.transform.position = silhouettePosition;
    }
}
