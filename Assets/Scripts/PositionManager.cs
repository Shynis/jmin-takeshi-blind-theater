using OpenCvSharp;
using OpenCvSharp.Demo;
using UnityEngine;
using Rect = OpenCvSharp.Rect;

public class PositionManager : MonoBehaviour
{
    [SerializeField] private float time_delay = 0.5f;
    private float time_left = 0f;

    public Vector2 headPosition {get; private set;}
    public bool headFound {get; private set;}

    private FaceDetectorScene faceDetectorScene;

    // Start is called before the first frame update
    private void Awake()
    {
        faceDetectorScene = FindObjectOfType<FaceDetectorScene>();
    }

    private void Start()
    {
        headPosition = new Vector2(0, 0);
    }

    // Update is called once per frame
    private void Update()
    {
        GetFacesInCamera();
    }

    private void GetFacesInCamera()
    {
        Vector2 textureSize = faceDetectorScene.TextureSize;
        Vector2 factor = new Vector2(Screen.width / textureSize.x, Screen.height / textureSize.y);

        if(faceDetectorScene.Faces.Count > 0 )
        {
            Rect rect = faceDetectorScene.Faces[0];

            // Debug.Log("---------------------------");
            // Debug.Log("rect raw: " + rect.Center.X + ", " + rect.Center.Y);

            rect.Size = new Size(rect.Size.Width * factor.x, rect.Size.Height * factor.y);
            rect.X = (int) (rect.X * factor.x);
            rect.Y = Screen.height - (int) (rect.Y * factor.y);

            headPosition = Camera.main.ScreenToWorldPoint(new Vector3(rect.Center.X, rect.Center.Y, 0));

            // Debug.Log("rect changed: " + rect.Center.X + ", " + rect.Center.Y);

            headFound = true;

            time_left = time_delay;
        }
        else
        {
            headFound = false;

            time_left -= Time.deltaTime;
            if(time_left < 0)
            {
                // Pause game with a message saying that they left the zone
            }
        }
    }
}
