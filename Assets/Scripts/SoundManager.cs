using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable] public struct AudioElement
{
    public string name;
    public AudioClip clip;
    public float volume;
}


public class SoundManager : MonoBehaviour
{
    private SoundManager m_instance;
    public SoundManager instance { get { return m_instance; } private set { m_instance = instance; } }


    [SerializeField] private AudioSource m_audioSource1;

    [SerializeField] private List<AudioElement> m_sfxList;
    public Dictionary<string, AudioElement> sfxDictionnary;


    // Start is called before the first frame update
    void Awake()
    {
        sfxDictionnary = new Dictionary<string, AudioElement>();
        foreach (AudioElement sfx in m_sfxList)
        {
            sfxDictionnary.Add(sfx.name, sfx);
        }
    }

}
